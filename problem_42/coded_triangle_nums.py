import sys

def main():
	f = open('p042_words.txt', 'r')

	letters = {'A' : 1, 'B' : 2, 'C' : 3, 'D' : 4, 'E':5, 'F':6, 'G':7, 'H':8, 'I':9, 'J':10, 'K':11, 'L':12, 'M':13, 'N':14, 'O':15,
				'P':16, 'Q':17, 'R':18, 'S':19, 'T':20, 'U':21, 'V':22, 'W':23, 'X':24, 'Y':25, 'Z':26}

	number_of_triwords = 0
	tri_nums = []
	for n in range(1, 1000):
		tri_nums.append(0.5 * n * (n + 1))
	# this is done because searching sets is O(1) time
	tri_nums = set(tri_nums)

	names = []
	for line in f:
		names = line.split(",")
		for name in names:
			name_score = 0
			for letter in name[1:len(name)-1]:
				name_score += letters[letter]
			if name_score in tri_nums:
				number_of_triwords += 1

	print number_of_triwords
	
if __name__ == "__main__":
	main()