import numpy as np
import sys

def main():
    i = 600851475143L
    
    factors = []    
    for n in range(2, sys.maxint/100):
        if i % n == 0 and isPrime(n):
            factors.append(n)
    print factors

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True

if __name__ == "__main__":
    main()
