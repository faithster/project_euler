import sys
import numpy as np

def main():
    f = open('ints.txt')
    lines = f.readlines()
    
    nums = []
    for line in lines:
        nums.append(line.split())
    nums = np.array(nums)
    
    max_prod = -sys.maxint
    prod = 1
    
    # check horizontal products of adajacent four numbers
    for i in range(len(nums)):
        for j in range(len(nums)):
            if j + 3 < len(nums):
                prod = 1
                for adj in range(4):
                    prod *= int(nums[i][j + adj])
                    if prod > max_prod:
                        max_prod = prod
                            
    # check vertical products of adajacent four numbers
    for j in range(len(nums)):
        for i in range(len(nums)):
            if i + 3 < len(nums):
                prod = 1
                for adj in range(4):
                    prod *= int(nums[i + adj][j])
                    if prod > max_prod:
                        max_prod = prod
                            
    # check diagnol products from left to right of four adjacent numbers
    for i in range(len(nums)):
        for j in range(len(nums)):
            if j + 3 < len(nums) and i + 3 < len(nums):
                prod = 1
                for adj in range(4):
                    #print i + adj, j + adj
                    prod *= int(nums[i + adj][j + adj])
                    if prod > max_prod:
                        max_prod = prod
                #print " "
                            
    # check diagnol products from right to left of four adjacent numbers
    for i in range(len(nums)):
        for j in range(len(nums)-1, -1, -1):
            if j - 3 >= 0 and i + 3 < len(nums):
                prod = 1
                for adj in range(4-1, -1, -1):
                    #print i + adj, j - adj
                    prod *= int(nums[i + adj][j - adj])
                    if prod > max_prod:
                        max_prod = prod
                #print " "
    
    print max_prod
            
if __name__ == "__main__":
    main()
