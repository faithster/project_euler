def main():
    large_num = 0
    for a in range(1, 100):
        for b in range(1, 100):
            num = a ** b
            if sum_digits(num) > large_num:
                large_num = sum_digits(num)
    print large_num

def sum_digits(num):
    sum = 0
    for digit in str(num):
        sum += int(digit)
    return sum

if __name__ == "__main__":
    main()
