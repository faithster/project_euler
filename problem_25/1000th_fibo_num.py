
def main():
    Fn1 = 1
    Fn2 = 1
    
    for i in range(3, 12000):
        Fn = Fn1 + Fn2
        Fn2, Fn1 = Fn1, Fn
        
        if len(str(Fn)) >= 1000:
            print i
            break
            
if __name__ == "__main__":
    main()
