
# this problem was fun! took a while but solved by analying outcome of smaller triangle at each turn. Analyses triangle of 6 elements rather than entire tree.

def main():

    triangle = [
        [75],
        [95, 64],
        [17, 47, 82],
        [18, 35, 87, 10],
        [20, 04, 82, 47, 65],
        [19, 01, 23, 75, 03, 34],
        [88, 02, 77, 73, 07, 63, 67],
        [99, 65, 04, 28, 06, 16, 70, 92],
        [41, 41, 26, 56, 83, 40, 80, 70, 33],
        [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
        [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
        [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
        [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
        [63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
        [04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23]]
    
    # used a the base case to for testing
    '''triangle = [
        [3],
        [7, 4],
        [2, 4, 6],
        [8, 5, 9, 3]]
    '''
    
    mov = 0
    right = 0
    left = 0
    sum = triangle[0][mov]
    print 75
    for i in range(1, len(triangle)):
        #print mov
                
        if mov + 1 < len(triangle[i]):
            right = triangle[i][mov+1]
        else:
            right = -1
            
        left = triangle[i][mov] 
           
         # check one ahead
        if i + 1 < len(triangle):
            leftleft = triangle[i + 1][mov]
            
            if mov + 1 < len(triangle[i + 1]):
                leftright = triangle[i+1][mov+1]
            else:
                leftright = -1
                
            
            if mov + 2 < len(triangle[i + 1]):
                rightright = triangle[i+1][mov+2]
            else:
                rightright = -1
                
            ll = left + leftleft
            lr = left + leftright
            rr = right + leftright
            rr2 = right + rightright
            
            gain = max((ll, 'll'), (lr, 'lr'), (rr, 'rr'), (rr2, 'rr2'))
            
            if gain[1] == 'll':
                mov = mov
            elif gain[1] == 'lr':
                mov = mov
            elif gain[1] == 'rr' or gain[1] == 'rr2':
                mov += 1

        else:
            mov += 1
            
        print triangle[i][mov]
        sum += triangle[i][mov]
    print sum
    
if __name__ == '__main__':
    f = open('SymbolTables.txt', 'r')
    main()
