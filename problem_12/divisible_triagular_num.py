import numpy as np
from math import sqrt

def main():
    
    ith_tri_num = 1
    while (True):
        tri_num = ith_tri_num * (ith_tri_num + 1)/2 # n(n+1)/2
        #tri_num = sum(range(ith_tri_num + 1)) really inefficient
        num = num_factors(tri_num)
        print tri_num, num
        if num > 500:
            print tri_num
            break
        ith_tri_num += 1

def num_factors(num):
    factors = []
    incr = 1
    
    # check if number if odd, then no even number can be factor
    if num % 2 != 0:
        incr = 2
    
    for i in range(1, int(sqrt(num) + 1)):
        if num % i == 0:
            # remember factors occur in pairs and largest will
            # not be greater than sqrt(n)
            factors.append(i)
            factors.append(num//i)
    factors.append(num)
    return len(factors)
    
# Super fucking efficient factorizing algorithm cant work out what it is?
def factor(n):
	a, r = 1, [1]
	while a * a < n:
		a += 1
		if n % a: continue
		b, f = 1, []
		while n % a == 0:
			n //= a
			b *= a
			f += [i * b for i in r]
		r += f
	if n > 1: r += [i * n for i in r]
	return len(r)    
    
if __name__ == "__main__":
    main()
