import numpy as np

def main():
    num = 2
    count = 0
    while (True):
        if isPrime(num):
            count += 1
            if count == 10001:
                print num
                break
        num += 1
    
def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True


if __name__ == "__main__":
    main()
