import math

def main():

	results = []
	for i in range(11, 10000001):
		num = str(i)
		cnt = 0
		for digit in num:
			if cnt > i:
				break
			cnt += math.factorial(int(digit))

		if cnt == i:
			results.append(i)

	print results

if __name__ == "__main__":
	main()