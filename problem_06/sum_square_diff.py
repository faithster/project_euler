import numpy as np

def main():
    sum_sq = 0
    sq_sum = 0
    
    for i in range(1, 101):
        sum_sq += i ** 2
        sq_sum += i
    print (sq_sum ** 2) - sum_sq
        
if __name__ == "__main__":
    main()
