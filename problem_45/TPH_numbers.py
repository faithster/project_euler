import numpy as np
import matplotlib.pyplot as plt

def main():

	Tn = []
	Pn = []
	Hn = []
	TnAndPn = []

	for n in range(1, 100000):
		Tn.append(n * (n + 1) / 2)
		Pn.append(n * (3 * n - 1) / 2)
		Hn.append(n * (2 * n - 1))

	Tn = set(Tn)
	Pn = set(Pn)
	Hn = set(Hn)
	for tri in Tn:
		if tri in Pn and tri in Hn:
			TnAndPn.append(tri)

	print TnAndPn
	
if __name__ == "__main__":
	main()