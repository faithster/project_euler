import sys

def main():
	max = -sys.maxint
	for i in range(2, 10000000):
		if isPrime(i):
			if isPandigital(i, len(str(i))):
				if i > max:
					max = i
	print max

def isPandigital(i, len):
	num = ''
	for j in range(1, len + 1):
		num += str(j)
	sorted_num = sorted(num)
	sorted_i = sorted(str(i))

	if sorted_num == sorted_i:
		return True
	return False

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True

if __name__ == "__main__":
	main()