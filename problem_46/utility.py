def main():
	odd_composite_numbers = [i for i in range(7994001, 17991001 + 1) if not isPrime(i) and not i % 2 == 0]
	for com_num in odd_composite_numbers:
		print com_num

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True

if __name__ == "__main__":
	main()
