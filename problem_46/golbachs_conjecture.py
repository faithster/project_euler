import multiprocessing as mp
import numpy as np

def main():
	global squares
	primes = [i for i in range(2, 6000) if isPrime(i)]
	squares = [i * i for i in range(1, 2000)]
		
	f = open('odd_comp_numbers_under_7994001c.txt')	
	odd_composite_numbers = []	
	for odd_comp_num in f:
		odd_composite_numbers.append(int(odd_comp_num))		

	pool = mp.Pool(processes=4)
	computed_ocn = pool.map(compute_odd_com_nums, primes)
	computed_ocn_flat = np.array(computed_ocn)
	computed_ocn_flat = np.hstack(computed_ocn_flat)
	#print computed_ocn_flat	
	
	#print odd_composite_numbers	
	#print computed_ocn

	#print computed_ocn
	#print max(computed_ocn)
	#
	print min(set(odd_composite_numbers) - set(computed_ocn_flat))
	
def compute_odd_com_nums(prime):
	global squares
	computed_ocn = []
	for square in squares:
		# check that number is composite before adding it to list
		if not isPrime(prime + 2 * square) and not (prime + 2 * square) % 2 == 0:
			computed_ocn.append(prime + 2 * square)
	return computed_ocn

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True 

if __name__ == "__main__":
	main()
