import numpy as np
import sys

def main():
    # read in file to memory
    f = open('ints.txt')
    lines = f.readlines()
    
    nums = []
    long_list = "" 
    for line in lines:
        for i in line:
            if i != ' ' and i != '\n':
                nums.append(i)
    
    max = -sys.maxint
    prod = 1
    for i in range(len(nums)):
        prod = 1
        if i < (len(nums) - 13):
            for j in range(13):           
                prod *= int(nums[i + j])
                if (prod > max):
                    max = prod
    print max
                  
if __name__ == "__main__":
    main()
