
def main():
	dbase_palindromes = []
	for i in range(1, 1000000):
		if isPalindrome(int(bin(i)[2:])) and isPalindrome(i):
			dbase_palindromes.append(i)
	print sum(dbase_palindromes)

def isPalindrome(num):
    size = getLength(num)
    num_arr = populateArray(num, size)
    
    for i in range(len(num_arr)/2):
        if (num_arr[i] != num_arr[len(num_arr) - 1 - i]):
            return False
    return True
    
def populateArray(num, size):
    num_arr = []
    k = 10 ** (size - 1)
    for i in range(size):
        num_arr.append((num / k) % 10)
        k = k/10 
    return num_arr  

def getLength(num):
    count = 0
    val = 10
    l = num/10
    while (l != 0):
        l = num / val
        val *= 10
        count += 1
    return count

if __name__ == "__main__":
	main()