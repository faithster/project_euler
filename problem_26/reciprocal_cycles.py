import sys
from decimal import *

def main():
	# set decimal precision
	getcontext().prec = 2000
	tokens_list_list = []
	max_token = str()
	d = 0
	for i in range(11, 1000):
		# make sure there are 40 digits after decimal point
		dec = Decimal(1)/Decimal(i)
		str_dec = str(dec)
		str_dec = str_dec[2:]
		# ignore trivial decimal
		if len(str_dec) > 2:
			tokens = []
			j = 0
			while j < 1000:
				token = str_dec[:j]
				if token == str_dec[len(token):2*len(token)] and not isUniform(token):
					if len(token) > len(max_token):
						max_token = token
						d = i
					break
				j += 1
	
	print d, max_token

# check to see if the longest cycle is repeating with a single value
# ie. 0.11111111111111111111. this doesn't count
def isUniform(longest_cycle):
	for i in range(len(longest_cycle)):
		if longest_cycle[0] != longest_cycle[i]:
			return False
	return True

if __name__ == "__main__":
	#print isUniform("111111")
	main()
