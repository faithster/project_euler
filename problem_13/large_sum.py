import sys
import numpy as np

def main():
    # read in file to memory
    f = open('ints.txt')
    lines = f.readlines()
    
    # store numbers in array
    nums = []
    for line in lines:
        nums.append(line)
    
    carry_over = 0
    for j in range(49, -1, -1):
        col_sum = carry_over
        for i in range(len(nums)):
            col_sum += int(nums[i][j])
        print col_sum
        carry_over = col_sum // 10
    
    print carry_over
    
        
if __name__ == "__main__":
    main()
