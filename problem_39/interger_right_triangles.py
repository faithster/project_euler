import multiprocessing as mp
import sys

def main():
	p = range(200, 300)

	# if you have 4 cores, why not use the motherfuckers?
	#pool = mp.Pool(processes=4)
	#max_p_vals = pool.map(maxp, p)
	#print max_p_vals

	#max_val = sorted(max_p_vals, key=maximum_solution)
	#print max_val[len(max_val)-1]

	# lookup table :)
	perims = {}
	for p in range(1, 1001):
		perims[str(p)] = None

	maxp_alt(1001, perims)
	#print perims

	max_sol = (0, -sys.maxint)
	#print sorted(perims, key=maximum_solution)	
	for key in perims:
		if perims[key] != None:
			print key
			if len(set(perims[key])) > max_sol[1]:
				max_sol = (key, len(set(perims[key]))) 
	print max_sol


# custom sorting - so easy in python it makes me want to shed a tear ;`)
def maximum_solution(tup):
	return len(tup)

# computes for max number of combinations
def maxp(p):
	""" DEPRECATED """
	maxp = (0, 0)

	aa = 0
	bb = 0
	perim = []
	for a in range(1, p):
		for b in range(1, p):
			for c in range(1, p):
				sides = [a,b,c]
				maxs = max(sides)
				aa, bb = getSides(sides, maxs)
				if isRightAngled(aa, bb, maxs) and not aa == bb and not bb == maxs:
					if aa + bb + maxs == p:
						set_lens = sorted([aa, bb, maxs])
						if not set_lens in perim:
							perim.append(set_lens)
	if len(perim) > maxp[1]:
		maxp = (p, len(perim))
	return maxp
	
def maxp_alt(p, perims):
	""" Another solution using a dictionary (symbol table). This means that the following 
	loop does not run through for each value of p and thereby recompute iterations. It only 
	runs through once finding right angle triangles and adding there perim sums to the dict

	MUCH more efficient
	"""
	for a in range(1, p/2):
		for b in range(1, p/2):
			for c in range(1, p/3):
				if not a + b + c > p:
					sides = [a, b, c]
					maxs = max(sides)
					aa, bb = getSides(sides, maxs)
					if isRightAngled(aa, bb, maxs) and not aa == bb and not bb == maxs:
						#print aa, bb, maxs
						# if no perim exists for p add one
						p_hyp = aa + bb + maxs # new perimeter
						if perims[str(p_hyp)] == None:
							perims[str(p_hyp)] = [(aa, bb, maxs)]
						# append perim sum to existing list
						else:
							perims[str(p_hyp)].append((aa,bb,maxs))

	return perims

def getSides(sides, maxs):
	a = 0
	b = 0
	if maxs == sides[0]:
		a = sides[1]
		b = sides[2]
	elif maxs == sides[1]:
		a = sides[0]
		c = sides[2]
	else:
		a = sides[0]
		b = sides[1]
	return a, b	

def isRightAngled(a, b, hyp):
	if (((a ** 2) + (b ** 2)) ** 0.5) == hyp:
		return True
	return False

if __name__ == "__main__":
	main()