
def main():
	l = largest_common_mult(20, 30)
	print l
	print type(l), type(10.0)
	print '%.15f' % l 
	if l == 10.0:
		print "???"

def largest_common_mult(numerator, denominator):
	lcm = -1
	i = 0.001
	while i < numerator - 0.001:
		if numerator%i < 1.0e-4 and denominator%i < 1.0e-4:
			if i > lcm:
				print '%.15f' % i
				lcm = i
		i += 0.001
	return lcm

if __name__ == "__main__":
	main()