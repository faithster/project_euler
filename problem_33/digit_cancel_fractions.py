""" weird error, 49%12.25 in terminal = 0, but during runtime it
	equalled 8.3e-13 ?? runtime vs interpretor use different format
	types? makes sense considering interpretor is more fluffy	
"""

def main():
	for numerator in range(10, 100):
		for denominator in range(10, 100):
			# check that fraction is below 1.0
			if 1.0 * numerator/denominator < 1.0:
				#print numerator, denominator
				multiples = []
				multiples = largest_common_mult(numerator, denominator)
				for lcm in multiples:
					if not str(lcm) == str(10.0) and not lcm == None:
						# accout for floating point weirdness
						if str(int(numerator/lcm)) in str(numerator) and str(int(denominator/lcm)) in str(denominator):
						# less than 1?
							# get the number removed from the double digit num
							if isCancelled(str(numerator), str(int(numerator/lcm)), str(denominator), str(int(denominator/lcm))):
								print lcm, numerator, denominator, "--", int(numerator/lcm), int(denominator/lcm)
							

def isCancelled(numerator, num_can, denominator, denom_can):
	if numerator[0] == num_can:
		can_num = numerator[1]
	else:
		can_num = numerator[0]

	if denominator[0] == denom_can:
		can_dem = denominator[1]
	else:
		can_dem = denominator[0]

	if can_num == can_dem:
		#print numerator, denominator, can_num
		return True
	return False	
			
def largest_common_mult(numerator, denominator):
	lcm = -1
	i = 0.0001
	multiples = []
	while i < numerator:
		#change to 1.0e-4 to see other 3
		if numerator%i < 1.0e-4 and denominator%i < 1.0e-4:
			if i > lcm:
				lcm = i
				multiples.append(lcm)
		i += 0.0001
	return multiples

if __name__ == "__main__":
	main()