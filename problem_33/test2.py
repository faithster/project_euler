# works out lowest common denominator #1337
import sys

def main():
	nums = [64, 65, 98, 95]
	for i in range(1, 5000000):
		if lcm(nums, i):
			print i
			sys.exit(0)

def lcm(nums, i):
	for num in nums:
		if not i % num == 0:
			return False
	return True
			
if __name__ == "__main__":
	main()