
def main():

    for x in range(1, 1000000):
        if  is_permutation(x, 2*x) and \
            is_permutation(x, 3*x) and \
            is_permutation(x, 4*x) and \
            is_permutation(x, 5*x) and \
            is_permutation(x, 6*x):
                print "Lowest: ", x, 2*x, 3*x, 4*x, 5*x, 6*x
                break

    print is_permutation(125874, 251748)

def is_permutation(num, perm_num):
    num = str(num)
    perm_num = str(perm_num)

    if not len(num) == len(perm_num):
        return False

    for i in num:
        if not i in perm_num:
            return False
    return True

if __name__ == "__main__":
    main()
