"""recursive goodness ;)"""

total_sum = 0
cobo = []

def main():
	global total_sum
	chain = ""

	coin_sum(0, 0, chain)
	print total_sum

def coin_sum(coin_s, new_coin, chain):	
	global total_sum
	# generate chain
	if not new_coin == 0:
		chain += str(new_coin)
		chain = ''.join(sorted(chain))
		if chain in set(cobo):
			return
		else:
			cobo.append(chain)
	#print chain
	#print cobo

	coin_s = coin_s + new_coin
	print total_sum
	if coin_s == 200:
		# if not add to list of cobos
		total_sum += 1
		return
	elif coin_s > 200:
		return

	coin_sum(coin_s, 1, chain)
	coin_sum(coin_s, 2, chain)
	coin_sum(coin_s, 5, chain)
	coin_sum(coin_s, 10, chain)
	coin_sum(coin_s, 20, chain)
	coin_sum(coin_s, 50, chain)
	coin_sum(coin_s, 100, chain)
	coin_sum(coin_s, 200, chain)

if __name__ == "__main__":
	main()
