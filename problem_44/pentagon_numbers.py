import sys

def main():
	pent_numbers = []
	for n in range(1, 10000):
		pent_numbers.append(n * (3 * n - 1) / 2)

	pent_numbers = set(pent_numbers)

	possibles = []
	min = sys.maxint
	for j in pent_numbers:
		for k in pent_numbers:
			if j - k in pent_numbers and j + k in pent_numbers and abs(j - k) < min:
				possibles = abs(k - j)
				min = possibles
	print min
	
if __name__ == "__main__":
	main()