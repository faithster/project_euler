import numpy as np
import sys
import time

def main():
	start_num = 0
	max_len = -sys.maxint
	for i in range(2, 1000000):
		len_sq = collatz_chain(i)
		if len_sq > max_len:
			max_len = len_sq
			start_num = i

	print start_num, len_sq

def collatz_chain(num):
	count = 1
	while (num != 1):
		if num % 2:
			num = 3*num + 1
			count += 1
		else:
			num = num/2
			count += 1
	return count

if __name__ == "__main__":
	now = time.time()
	main()
	print "Elapsed time: ", time.time() - now