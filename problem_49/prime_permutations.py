import sys
from itertools import permutations

def main():
	# 4 digit starting number
	for start_num in range(1000, 9997):
		num = 0
		chain = []
		perms = [''.join(p) for p in permutations(str(start_num))]
		s = set(perms)

		# number to add
		for add_num in range(1, 4000):
			chain.append(str(start_num))
			num = start_num
			for i in range(2):
				num += add_num
				chain.append(str(num))
			if isTheOne(chain, s):
				print chain, add_num
			chain = []

def isTheOne(chain, s):
	for num in chain:
		if not isPrime(int(num)) or not num in s:
			return False
	return True

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True

if __name__ == "__main__":
	main()
