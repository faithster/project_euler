'''~ brute force ~ is there any other way? :) '''
import multiprocessing as mp
from sys import *

def main():
	global primes, len_primes		# stores list of primes
	max_con_sum = (0, 0)	# stores tuples (prime, consecutive sum list length)
	# populate list of prime numbers
	primes = [num for num in range(2, 1000000) if isPrime(num)]		
	len_primes = len(primes)

	# multiprocessing over 4 cores
	pool = mp.Pool(processes=4)
	chains = pool.map(consecutive_pr, primes)

	sl = sorted(chains, key=max_len)
	print sl[len(sl)-1]

def max_len(tup):
	return tup[0][1]

def consecutive_pr(num):
	global primes, len_primes
	# add consecutive primes
	keep_adding = True
	consecutive_chains = []
	for i in range(len_primes):
		# start summing consecutive primes from i
		j = i
		consecutive_primes_sum = (0, 0)
		while j < len_primes and keep_adding:
			consecutive_primes_sum = (primes[j] + consecutive_primes_sum[0], consecutive_primes_sum[1] + 1)
			if consecutive_primes_sum[0] == num:
				consecutive_chains.append(consecutive_primes_sum)
				# make sure only one chain is created
				if len(consecutive_chains) == 1:
					keep_adding = False
				break
			elif consecutive_primes_sum[0] > num:
				break
			j += 1
	return consecutive_chains

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True 

if __name__ == "__main__":
	main()