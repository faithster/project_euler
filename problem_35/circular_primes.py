
def main():
	circular_primes = 0

	for num in range(2, 1000000):
		i = num
		if isPrime(num):
			while (True):
				num = rotate(str(num))
				if not isPrime(int(num)):
					break
				if (int(num) == i):
					circular_primes += 1
					break
	print circular_primes


def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True

def rotate(num):
	num_array = populateArray(num)

	if len(str(num)) > 1:
		stor = num_array[-1]
		num_array[-1] = num_array[0]

		for i in range(1, len(num_array) - 1):
			num_array[i - 1] = num_array[i]
		num_array[-2] = stor

		n = ''
		for i in num_array:
			n += str(i)
		return n
	else:
		return str(num)


def populateArray(num):
    num_arr = []
    for i in num:
    	num_arr.append(int(i))
    return num_arr  

def getLength(num):
    count = 0
    val = 10
    l = num/10
    while (l != 0):
        l = num / val
        val *= 10
        count += 1
    return count

if __name__ == "__main__":
	main()