

def main():

	dict = {"0" : 31, "1" : 28, "2" : 31, "3" : 30, "4" : 31, "5" : 30, "6" : 31, "7" : 31, "8" : 30, "9" : 31, "10" : 30, "11" : 31}
	days = ["m", "t", "w", "th", "fr", "sa", "su"]

	sundays = 0
	dayOWeek = 0
	for year in range(1900, 2000 + 1):
		# check if leap year
		if isLeapYear(year):
			dict['1'] = 29
		else:
			dict['1'] = 28

		for month in range(12):
			
			for day in range(0, dict[str(month)]):
				# Check if the day is a sunday and that it is indeed the first of the month
				if days[dayOWeek] == "su" and day == 0:
					if year != 1900:
						sundays += 1
				dayOWeek += 1
				dayOWeek %= 7

	print sundays

def isLeapYear(year):
	if year % 4 == 0:
		if str(year)[2:4] == "00" and year % 400 == 0:
			return True
		elif str(year)[2:4] != "00":
			return True
	else:
		return False

if __name__ == "__main__":
	main()