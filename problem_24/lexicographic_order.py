from itertools import permutations

def main():
	perms = [''.join(p) for p in permutations('0123456789')]
	s = set(perms)

	nums = []
	for i in s:
		nums.append(int(i))

	sorte = sorted(nums)
	print sorte[999999]

if __name__ == "__main__":
	main()