from math import factorial

def main():
    count = 0
    for n in range(1, 101):
        for r in range(1, 101):
            if n >= r and len(str(nCr(n, r))) >= 7:
                count += 1
    print count

def nCr(n, r):
    return factorial(n) / (factorial(r) * factorial(n-r))

if __name__ == "__main__":
    main()
