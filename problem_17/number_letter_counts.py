
def main():
    # base numbers
    dict = {1 : 'one', 2 : 'two', 3 : 'three', 4 : 'four', 5 : 'five', 6 : 'six', 7 : 'seven', 8 : 'eight', 9 : 'nine', 10 : 'ten', 11 : 'eleven', 12 : 'twelve', 13 : 'thirteen', 14 : 'fourteen', 15 : 'fifteen', 16 : 'sixteen', 17 : 'seventeen', 18 : 'eighteen' , 19 : 'nineteen'}
    
    sum = 0
    for i in range(1, 1001):
        number = str(i)
        
        # small numbers < 10
        if len(number) < 2:
            num = dict[i]
            sum += len(num)
            print num
        elif len(number) == 2:
            num = numberLtTwo(number, i, dict)
            sum += len(num)
            print num
            
        if len(number) == 3:
                if number[0] == '1':
                    if number[1] == '0' and number[2] == '0':
                        num = "onehundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "onehundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
                elif number[0] == '2':
                    if number[1] == '0' and number[2] == '0':
                        num = "twohundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "twohundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
                elif number[0] == '3':
                    if number[1] == '0' and number[2] == '0':
                        num = "threehundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "threehundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
                elif number[0] == '4':
                    if number[1] == '0' and number[2] == '0':
                        num = "fourhundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "fourhundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict) 
                        sum += len(num)
                        print num
                elif number[0] == '5':
                    if number[1] == '0' and number[2] == '0':
                        num = "fivehundred" 
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "fivehundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
                elif number[0] == '6':
                    if number[1] == '0' and number[2] == '0':
                        num = "sixhundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "sixhundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
                elif number[0] == '7':
                    if number[1] == '0' and number[2] == '0':
                        num = "sevenhundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "sevenhundredand" + numberLtTwo(str(stripedNumber),stripedNumber, dict) 
                        sum += len(num)
                        print num
                elif number[0] == '8': 
                    if number[1] == '0' and number[2] == '0':
                        num = "eighthundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "eighthundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
                elif number[0] == '9':    
                    if number[1] == '0' and number[2] == '0':
                        num = "ninehundred"
                        sum += len(num)
                        print num
                    else:
                        stripedNumber = int(number[1:])
                        num = "ninehundredand" + numberLtTwo(str(stripedNumber), stripedNumber, dict)
                        sum += len(num)
                        print num
        elif len(number) == 4:
            num = "onethousand"
            sum += len(num)
            print num
    print sum
def numberLtTwo(number, i, dict):
    if len(number) == 1:
        return dict[i]
    
    if len(number) == 2:
        if number[0] == '1':
            return dict[i]
        elif number[0] == '2':
            if number[1] == '0':
                return 'twenty'
            else:
                return 'twenty' + dict[int(number[1])]
        elif number[0] == '3':
            if number[1] == '0':
                return 'thirty'
            else:
                return 'thirty' + dict[int(number[1])]
        elif number[0] == '4':
            if number[1] == '0':
                return 'forty'
            else:
                return 'forty' + dict[int(number[1])]
        elif number[0] == '5':
            if number[1] == '0':
                return 'fifty'
            else:
                return 'fifty' + dict[int(number[1])]
        elif number[0] == '6':
            if number[1] == '0':
                return 'sixty'
            else:
                return 'sixty' + dict[int(number[1])]
        elif number[0] == '7':
            if number[1] == '0':
               return 'seventy'
            else:
               return 'seventy' + dict[int(number[1])]
        elif number[0] == '8':
            if number[1] == '0':
                return 'eighty'
            else:
                return 'eighty' + dict[int(number[1])]
        elif number[0] == '9':
            if number[1] == '0':
                return 'ninety'
            else:
                return 'ninety' + dict[int(number[1])]    
    
if __name__ == "__main__":
    main()
