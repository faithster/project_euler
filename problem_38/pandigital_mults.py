from itertools import permutations
import sys

def main():
	perms = [''.join(p) for p in permutations('123456789')]
	s = set
	max = -sys.maxint

	for num in range(1, 10000):
		concat = ''
		for mult in range(1, 10000):
			concat += str(num * mult)
			if len(concat) >= 9:
				break
		# check if pandigital number
		if concat in perms:
			# check if bigger than previous result
			if (int(concat) > max):
				max = int(concat)
	print max


if __name__ == "__main__":
	main()