import sys

# Solution takes a bit longer than a minute to solve...

def main():
    j = 0
    i = 20
    while (True):
        print i
        if canDivide(i):
            print i
            break  
        i += 20
    print "BOOM"
    
def canDivide(num):
    for i in range(1, 21):
        if num % i != 0:
            return False
    return True

if __name__ == "__main__": 
    main()
