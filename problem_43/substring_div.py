from itertools import permutations

def main():
	perms = [''.join(p) for p in permutations('0123456789')]
	s = set

	list = []

	for perm in perms:
		d = 1
		count = 0
		for i in [2, 3, 5, 7, 11, 13, 17]:
			p1 = perm[d]
			p2 = perm[d+1]
			p3 = perm[d+2]
			if not int(p1 + p2 + p3) % i == 0:
				break
			d += 1
			if d == 8:
				list.append(int(perm))
	print sum(list)

if __name__ == "__main__":
	main()