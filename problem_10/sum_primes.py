import numpy as np

def main():
    sum_prime = 0
    for i in range(2, 2000000):
        if isPrime(i):
            sum_prime += i
    print sum_prime

def isPrime(num):
    n = (int) (num ** 0.5)
    for i in range(2, n + 1):
        if num % i == 0:
            return False
    return True
    
if __name__ == "__main__":
    main()
