import numpy as np

def main():
    num = 2 ** 1000
    num_string = str(num)
    count = 0
    for i in num_string:
        count += int(i)
    print count

if __name__ == "__main__":
    main()
