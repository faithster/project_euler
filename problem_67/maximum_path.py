import sys

# this problem was fun! took a while but solved by analying outcome of smaller triangle at each turn. Analyses triangle of 6 elements rather than entire tree.

def main():

    with open("SymbolTables.txt", 'r') as f:
        triangle = [map(int, line.split()) for line in f]
        
    # used a the base case to for testing
    '''triangle = [
        [3],
        [7, 4],
        [2, 4, 6],
        [8, 5, 9, 3]]
    '''
    
    mov = 0
    right = 0
    left = 0
    sum = triangle[0][mov]
    print 75
    for i in range(1, len(triangle)):
        #print mov
                
        if mov + 1 < len(triangle[i]):
            right = triangle[i][mov+1]
        else:
            right = -1
            
        left = triangle[i][mov] 
           
         # check one ahead
        if i + 1 < len(triangle):
            leftleft = triangle[i + 1][mov]
            
            if mov + 1 < len(triangle[i + 1]):
                leftright = triangle[i+1][mov+1]
            else:
                leftright = -1
                
            
            if mov + 2 < len(triangle[i + 1]):
                rightright = triangle[i+1][mov+2]
            else:
                rightright = -1
                
            ll = left + leftleft
            lr = left + leftright
            rr = right + leftright
            rr2 = right + rightright
            
            gain = max((ll, 'll'), (lr, 'lr'), (rr, 'rr'), (rr2, 'rr2'))
            
            if gain[1] == 'll':
                mov = mov
            elif gain[1] == 'lr':
                mov = mov
            elif gain[1] == 'rr' or gain[1] == 'rr2':
                mov += 1

        else:
            mov += 1
            
        print triangle[i][mov]
        sum += triangle[i][mov]
    print sum
    
if __name__ == '__main__':
    f = open('SymbolTables.txt', 'r')
    main()
