import sys

def main():
	trun_primes = 0
	trun_p_list = []
	i = 13
	while trun_primes != 11:
		if isPrime(i):
			if isTruncatable(i):
				print i
				trun_p_list.append(i)
				trun_primes += 1
		i += 1
	# print sum
	print sum(trun_p_list)
	

def isTruncatable(num):
	num_s = str(num)

	# from left to right
	for i in range(len(num_s)):
		#print int(num_s[i:])
		if not isPrime(int(num_s[i:])):
			return False
	# from right to left
	for i in range(len(num_s)):
		#print int(num_s[:i+1])
		if not isPrime(int(num_s[:i+1])):
			return False
	# must be truncatable
	return True

def isPrime(num):
	if num == 1:
		return False
	n = (int) (num ** 0.5)
	for i in range(2, n + 1):
		if num % i == 0:
			return False
	return True

if __name__ == "__main__":
	main()