import string
import multiprocessing as mp
import itertools

letters = []
file_number = 0

def main():
	ascii_lowercase = string.ascii_lowercase

	f = open("p059_cipher.txt")
	for line in f:
		for ascii_code in line.split(","):
			letters.append(int(ascii_code))

	passwords = list(itertools.permutations(ascii_lowercase, 3))

	for password in passwords:
		decrypt(password)

	#decrypt(('y', 'e', 's'))
	# multiprocessing over 4 cores
	#pool = mp.Pool(processes=4)
	#cleartext = pool.map(decrypt, passwords)

def decrypt(password):
	global file_number

	j = 0
	note = ''
	decrypted_letters = []

	for letter in letters:
		decrypted_letters.append(letter ^ ord(password[j % 3]))
		j += 1

	f = open("output/%s%s%s.txt" % (password[0], password[1], password[2]) , "w+")

	for decrypted_letter in decrypted_letters:
		f.write(str(unichr(decrypted_letter)))

	if ("g", "o", "d") == password:
		print sum(decrypted_letters)

	file_number += 1

if __name__ == "__main__":
	main()
