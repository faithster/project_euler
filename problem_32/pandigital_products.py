def main():
	pandigit = "123456789"
	pandigit_product = []

	for i in range(1, 1000):
		for j in range(1, 10000):
			product = i * j
			if len(str(i * j) + str(i) + str(j)) == 9:
				if product in pandigit_product:
					continue
				num = str(i) + str(j) + str(product)
				if isPandigital(sorted(num)):
					pandigit_product.append(product)
	print sum(pandigit_product)


def isPandigital(num):
	if num == ['1', '2', '3', '4', '5', '6', '7', '8', '9']:
		return True
	return False

if __name__ == "__main__":
	main()